## About me

![](images/avatar-photo.jpg)

Hi! Welcome to my blog, my name is Stanislas Mondesir. 
I am a student in computer science at ULB.
Visit this website to see my work!

## My background

### Previous work

Apart from my studies I work 6 month as a data analyst intern at Railnova, and before that I worked three month in Japan.

## Module Documentation

- [Gestion de projet et documentation](/fabzero-modules/module01/)
- [Conception Assistée par Ordinateur (CAO)](/fabzero-modules/module02/)
- [Impression 3D](/fabzero-modules/module03/)
- [Outil Fab Lab sélectionné](/fabzero-modules/module04/)
- [Dynamique de groupe et projet final](/fabzero-modules/module05/)

## Group Project 

Can be found [here](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-04)