# 3D impression

tool: prusa mk3s.
![imprimante](images/3Dprint.jpg)

## from code to object

The steps to transform a .scad model into a 3D-printed object are:  
export the scad to STL.  
![export VS](images/stl.png)

Import the STL in the software used to slice: PrusaSlicer  
![export](images/import.png)

Slice with the proper parameters, and export the gcode
![slice](images/slice.png)
![export gcode](images/gcode.png)

Put the gcode on an SD card. Then put the SD card in a 3d printer previously reserved on Fabman. Select the right file with the round buttons and begin the impression.

## Before impression

Before the impression, we need to be sure that the 3d printer is clean, and that there is no trace on the board, or pieces of plastic under it.

## Problem Faced during the impression

Nozzle clutter: sometimes the plastic can stick on the bus to fix this we stop the printing and try to remove the plastic. we can mount the head by going into settings.
![expl error](images/failedPrin.jpg)

## Flexkit

Before printing anything I need to be sure of the parameter, to do that I first print the measurement tool.
diameter measurement: begin on 4.8 in the center hole (the official Lego measure), and add or subtract 0.1. [scad](files/scad/mesureTool/hole.scad) [stl](files/stl/holeMesure.stl)

Ruler: ruler of every 1 and 10 units. [scad](files/scad/mesureTool/ruler.scad) [stl](files/stl/ruler2.stl)

Flex test: different rectangles with varying heights to see how the flexibility is affected. [scad](files/scad/mesureTool/Flexibility.scad) [stl](files/stl/Flexibility.stl)

![testers](images/testers.jpg)

And finally a simple piece with the proper measurements taken to be sure it worked.

Now I can change the parameter of the flexlinks which are at the top of each file.

### The kit

I know just have to print the different parts:

### fixed fixed beam

![flexlink](images/flexlink1.jpg)
[cad file](files/scad/fixed_fixed_Beam.scad)
[stl file](files/stl/fixed_fixed_Beam.stl)
![flexlink2](images/flexlink2.jpg)  

### fixed fixed initially curved

![flexlink](images/fixed_fixed_curved.jpg)
[cad file](files/scad/fixed_fixed_Beam_initialy_curve.scad)
[stl file](files/stl/fixed_fixed_Beam_initialy_curve.stl)

### coaxis

![flexlink](images/coaxis.jpg)
[cad file](files/scad/coaxis.scad)
[stl file](files/stl/coaxis.stl)

## The mechanism

### Idea

A gate that allows forces only in one direction.

### prototype

![mechanism](images/mechanism.jpeg)

### 3D model

![3D model](images/mechanism3D.png)
[cad file](files/scad/prototype.scad)
