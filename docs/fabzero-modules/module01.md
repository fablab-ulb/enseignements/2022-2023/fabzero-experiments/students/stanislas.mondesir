# 1. Project management and documentation

This week I worked on defining my final project idea and started to get used to the documentation process.

## Background

Being on Linux for some time now I did not have to install git and bash so those steps won't be part of this doc.

## git

git/git lab is a program permitting one to work in a group without risking deleting part of a project of a colleague.

The main command to know to manage your git project are:  

1. `git clone addresseGitRepo`: make a copy of the repository on your machine
2. `git add fileName`: Prepare the file to be staged during the next commit
3. `git commit -m "message"`: Commit the change made on your local branch  
4. `git push`: Update the repo on the server with your committed changes  
5. `git pull`: Update your local branch with the change on the server

### .gitignore

It is a setting file telling git what to ignore.

### Config Git Lab

- add ssh key: in 'user setting' go to ssh key and paste an ssh key found in ~/.ssh/*.pub or create it with ssh-keygen -t ed25519 -C "your_email@example.com"

## shell

main commands :  

- `cd`: 'change directory';  
- `ls`: 'show file';  
- `cat`: 'read a file';  
- `vim`: 'write';  
- `man`: 'get the manual of command';  
- `sudo`: 'give super user permission'  

### Path

the path is the route to find a file, the Linux path system works as an arborescence of folders beginning with `/` the root. To write a path you have two choices, to begin with, you either do an absolute path by starting with `/` or a relative path without starting with `/`. To make a route you just need to describe the folder needed to arrive at the destination file (or folder) separated with `/`.  
Special file:

- `.`: represent the current folder  
- `..`: represent the parent of the folder
- `~`: is an alias for the absolute path of your home.

example:

- `~/Document/src`
- `/home/user/Document/src`
- `Document/src` (with pwd=/home/user)
- `~/Document/../Document/../Document/src`
- `~/Document/././././src`all equivalent paths to src.

## Create a site

This WebPage is written in Markdown and built as an HTML with the program mkdocs: `mkdocs build`.

### Markdown

#### Basic Syntax

|element| syntax|
|----|---|
|header 1| # |
|header 2| ## |
|**bold** | \*\* text \*\* |
|list| - list item |
|`code` | \` code\`|
|```code block``` | \`\`\` code block \`\`\`|
|[link](#) | \[link\]\(href\) |

A useful tip not only for markdown is that `\\` escape a special character to write it literally.  
more [here](https://www.markdownguide.org/cheat-sheet)

#### Image Gallery

link an image:  

- from the server:  
`![](./images/fablab-machine-logos.svg)`  
![test server](./images/fablab-machine-logos.svg)  

- from a different server (! can be deleted or moved from the server and become a dead link)
`![](https://upload.wikimedia.org/wikipedia/en/7/7d/Lenna_%28test_image%29.png)`  
![test internet](https://upload.wikimedia.org/wikipedia/en/7/7d/Lenna_%28test_image%29.png)  

- If you work with svg you can even include the code:  

``` svg
<svg width="100" height="100">
  <circle cx="50" cy="50" r="40" stroke="green" stroke-width="4" fill="yellow" />
</svg>
```

<svg width="100" height="100">
  <circle cx="50" cy="50" r="40" stroke="green" stroke-width="4" fill="yellow" />
</svg>


#### Video  

The inclusion of video or other file coming from a different place from the internet are linked thanks to an iframe balize which embed document in the current html.

``` HTML

<iframe src="https://player.vimeo.com/video/10048961" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/10048961">Sound Waves</a> from <a href="https://vimeo.com/radarboy">George Gally (Radarboy)</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
```

<iframe src="https://player.vimeo.com/video/10048961" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/10048961">Sound Waves</a> from <a href="https://vimeo.com/radarboy">George Gally (Radarboy)</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

#### HTML

Markdown language natively understand HTML:  
Givin us the possibility to play with script and css

```HTML
<p class=cssTest>
	Hello
</p>
<button onClick="alert('HI')">say hi</button>
<style>
.cssTest{
	color: blue;
	background-color: red;	
}
.cssTest:hover{
	color: green;
}

</style>

```

<p class=cssTest>
	Hello
</p>
<button onClick="alert('HI')">say hi</button>
<style>
.cssTest{
	color: blue;
	background-color: red;	
}
.cssTest:hover{
	color: green;
}

</style>

## Compressing image

One goal during this project is to keep the repo size at a minimum.

### main file formats

- .svg: vectorial: ideal for icons diagram.
- .png: raster graphics with lossless compression better compression for screenshots of document
- .jpeg: raster graphics with Lossy compression better compression for pictures.

### tools

#### GUI  

- GIMP/ Photoshop: for raster graphics
- Inkscape/ AI: for vectorial graphics

#### Shell
imagemagick: a powerful tool for images.

- convert between file formats `convert imageIn.png` imageOut.jpg`
- change compression during conversion `convert -quality 90 fileIn fileOut`
- resize `convert imageIn -resize 50% imageOut`
- Grayscale `convert -type Grayscale imageIn imageOut`
- dithering `convert imageIn -colorspace gray -ordered-dither o8x8 imageOut`

### Comparison

Made on Lenna png seen above.  

- base png: 473 kb  
- conversion to jpg: 106kb  
- grayscale jpg: 68kb  
- jpeg with quality 70: 33kb  
- dithering: 11kb  

## Project management principle

The project management technique I will try to use all along the cursus is to plan the project as a series of components _Modular & Hierarchical Planning_ and work on them by the importance and available time using the _Supply-side Time Management_:  

1. Deciding how much "Total Time" is available per week.  
2. Break down in work session.  
3. Give an estimated amount of time to each task.  
4. Assign the task to the work session.  
And last but not least document every part of the project made.

## Setting everything up on Windows

I had to try to set up my repo on Windows to help others, so here are the steps I took.

### Windows on Linux

#### Virtualization

Logiciel: [virtualBox](https://www.virtualbox.org/)
iso: [windows_10]

First, download the iso. Add a new VM by clicking on new, and select the os you want to install. Start your new VM and select the iso you downloaded. Now make follow the windows installation steps.  

Error: 64 bits distro not available and `VT-x is disabled in the BIOS for all CPU modes`:
causes: my new hardware does not have virtualization enabled.
solution: boot in the bios with f12, del, etc depending on your computer, find the virtualization setting and enable it.  


### Using Git on Windows

#### Solution 1: Drop Windows and install Linux  

The easiest solution is to install Linux. Nowadays Linux is easy to install and work with, and doesn't have all the bloatware Windows have. Most Linux distributions have git and other useful coding software pre-installed or are easily installed through the packet manager.

#### Solution 2: Install git and git bash

Download [git](git-scm), (I set all parameters to default).  
set up your ssh key with ssh-keygen or use HTTPS to connect to your repo.

## Spelling mistakes

I have a problem not making spelling mistakes and to make this documentation without having to manually check each phrase I have to find a solution.

### Grammarly

Grammarly is an AI designed to correct the English of people. But it is an online tool so I have to find a way to use it as I write in my IDE. Thankfully there is an extension that brings it to VSCode [here](https://marketplace.visualstudio.com/items?itemName=znck.grammarly). Once it is installed I just had to go to the command interface of VS `ctrl+maj+p` and launch `Grammarly: check text`. It is not a perfect tool since it won't understand the difference between code and written text but it will grandly reduce the number of errors made.
