# 5. Dynamique de groupe et projet final

This week I worked on defining my final project idea and started to getting used to the documentation process.

## Tree

Analyse of the [dremelfuge](https://www.thingiverse.com/thing:1483https://www.thingiverse.com/thing:1483)
![Tree](images/arbre.png)

The dremelfuge tackle the cost and accessibility of medical tools which can reduce sanitary risque, and social divide.

## Create a group

To find a group we each had to take an object in link to a problematics that close to our heart. And after looking at everyones find the personnes that have a common related.  

## Find the thematics  

In the newly formed group we talk and talked to find common theme between our object then extrapolate the thematic.

My object was an ethernet cable to show the divide in internet access in education.

## Find the problematics

Each one at the time in the group needed to tell a problematic beginig the sentence by 'If I were you ...'.
When we have no more idea we have a speakperson go to other group to see what are their ideas in relation to our thematics.
Our main problematic was the lack/devide in access to education.

## Team dynamic tool

### Decision in group

- Consensus
- Random
- no objection
- majority vote
- jugement majoritaire
- temperature

## Equity in talking time

There are diffrent way to asure that everybody have the chance to say what they need to say.

- talking stick: an object is chosen as the talking stick and only the one with it can talk.
- limited talking time.
- talking turn: each one after the other say somthing; if nothing is to be said still need to say 'pass'.
- finger: if you need to say somthing instead of interupting you show your number in the queue.

## Template of a meeting

1. Weather report: each member tell how they are feeling, it is usefull so that poeple are less likely to take somthing personnaly.
2. Role: decide who will have which role during this reunion. 
3. Agenda of the meeting: say what will be talked during the meeting.
4. The meeting.
5. Aganda for the next meeting: what poeple will be doing for the next meeting
6. Weather report.
