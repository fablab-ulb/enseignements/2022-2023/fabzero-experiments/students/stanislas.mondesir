# 2. Computer-assisted designs

This week I worked on defining my final project idea and started to get used to the documentation process.

## Tool intro

- openSCAD: Drawing 3d CAD with code. To simplify my workflow I will use VSCode (/ VSCodium) with the openSCAD plugin.
- Inkscape: Drawing 2d vectorial image [.svg]  
- FreeCAD: Model 3D CAD.  

## Assignment

### Flexlink

For this week's assignment, we need to design a flexlink, which is a part used for prototyping compliant mechanisms with Lego.  

- [more about flexlink](https://www.compliantmechanisms.byu.edu/flexlinks)
- [more about compliant mechanisms](https://en.wikipedia.org/wiki/Compliant_mechanism)

### Parametric model

The CAD model needs to be parametric meaning it needs to be adjusted without the need to redesign the part, only by changing the parameter.

### Creative Commons

All our work needs to be used by at least the rest of the class, so we need the appropriate license. I have chosen the Creative Commons Attribution-ShareAlike 4.0 International License Given the right to my work if I am cited and that the new work still uses the same license.  
  
For it to be applicable in my code it needs to be in the header of my source file. To make it simpler for me I have added a snippet to my VS code generating automatically my header.
The steps to add a header are:  

1. On VSCode go to your command palette with `ctrl + Maj + P`  
2. Search for `snippets: Configure User Snippets`; then select the programming language you want the snippet to be associated with. Here I only make it for Scad.  
3. Add:  

Don't forget to change the body with your data.
choose
Now in your code VS code will propose you create the header when you type `header`

## First Model V2

- Openscad  
- Fixed-Fixed-Beam

### Lego Part

At first (in version 1) I used a rectangle and pushed the hole by making a difference with the cylinder. It was functional but not aesthetic.
In this version, I decided to make use of the hull function between the two extreme cylinders of the size of the hole plus a border to define.  
First I modeled the part for connecting to the Lego,
the idea is to make a rectangle with a hole, in other words (for Openscad) make a rectangle and delete from its cylinder. From which I still punch a hole with a difference of cylinder.

I know that this part will probably be needed multiple times so I decided to put it in a module so this script can be recalled in the future:

``` scad
nbHole = 2;

diametreHole = 10;
spaceBetween = 2;
border = 2;
lPHeight = 10;
rodeLenght = 40;
rodeHeight = 5;

realSpaceBetween = spaceBetween-border;


module makesuport(){
    difference(){
        hull(){
            cylinder(d=diametreHole+border, h=lPHeight);
            translate([(diametreHole+border+realSpaceBetween)*(nbHole-1), 0,0])
                cylinder(d=diametreHole+border, h=lPHeight);
        }
        for (i=[0:nbHole-1]){
            translate([(border + diametreHole + realSpaceBetween) * i,0,-1]){
                cylinder(h=lPHeight+5, d=diametreHole);
            }
        }
    }
}
```

![The 3d model of a two holes Lego part](./images/legoPart.png)  
[scad file](files/scad/legoPart.scad)

### The beam  

I need to make the beam connecting two "Lego parts", the parameters for the beam are the beam length, width, and height.
We know that the two Lego parts need to be "rodelenght" apart, so thanks to the module we can write and calculate beforehand the size of the Lego part so everything fits together.

``` scad
rodeWidth = border+diametreHole-border;
lPLenght = border+diametreHole*(nbHole)+spaceBetween*(nbHole-1);
center = border/2 + diametreHole/2;
makesuport();

translate([lPLenght-center-border/2, -rodeWidth/2,0])
    cube([rodeLenght+border, rodeWidth, rodeHeight]);


translate([lPLenght+rodeLenght, 0,0]) 
    makesuport();
```

![The 3d model a short fixed-fixed-beam](./images/fixed_fixed.png)
[scad file](files/scad/fixed_fixed_Beam.scad)

### Alternative to fixed-fixed-beam

#### cantilever-beam

I only need to delete one Lego part  
![The 3d model a short cantilever-beam](./images/cantilever.png)  
[scad file](files/scad/Cantilever_Beam.scad)

#### fixed-slotted-beam

Make one of the Lego parts connect the cylinder used for the difference with a hull.

``` scad
module makesuportSlotted(){
    difference(){
        hull(){
            cylinder(d=diametreTrou+border, h=rectHeight);
            translate([(diametreTrou+border+Realespace)*(nbTrou-1), 0,0])
                cylinder(d=diametreTrou+border, h=rectHeight);
        }
        hull(){
            cylinder(h=rectHeight+5, d=diametreTrou);
            translate([(border + diametreTrou + Realespace) * (nbTrou-1),0,-1])
                    cylinder(h=rectHeight+5, d=diametreTrou);
        }
    }
}
```

![The 3d model a short fixed-slotted-beam](./images/fixed_slotted.png)  
[scad file](files/scad/fixed_slotted_Beam.scad)

#### fixed fixed initially curve

Two Lego parts are connected by a curve line. I learned how to make an arc from @caterpillar on his blog found [here](https://openhome.cc/eGossip/OpenSCAD/SectorArc.html)

![img](images/fixed_fixed_curved_model.png)  
[scad file](files/scad/fixed_fixed_Beam_initialy_curve.scad)

#### coaxis

![img](images/coaxis_model.png)  
[scad file](files/scad/coaxis.scad)

## Measuring Tool

The machine we will use are not 100% precise, so it is a good idea to make measuring tools with their imprecision to use as a unit.

### Measuring hole size

For this piece, I modify the Lego part define before, where instead of the total number of holes the parameters are the number of holes to test with at each step we add and subtract from the diameter.

``` scad
nbTrouEachSide = 1;
BaseddiametreTrou = 10;
testStep = 0.1;
maxDiameter = BaseddiametreTrou + testStep * nbTrouEachSide;
espace = 2;
border = 2;
rectHeight = 10;

Realespace = espace-border;

module makesuport(){
    difference(){
        hull(){
            translate([(maxDiameter+border+Realespace)*-nbTrouEachSide, 0,0])
                cylinder(d=maxDiameter+border, h=rectHeight);
            translate([(maxDiameter+border+Realespace)*nbTrouEachSide, 0,0])
                cylinder(d=maxDiameter+border, h=rectHeight);
        }
        for (i=[-nbTrouEachSide:nbTrouEachSide]){
            translate([(border + maxDiameter + Realespace) * i,0,-1]){
                cylinder(h=rectHeight+5, d=BaseddiametreTrou+(i)*testStep);
            }
        }
    }
}
txt = str(str(BaseddiametreHole), ":" , str(testStep));
translate([0,-((border + maxDiameter)/2),0])
    rotate([90,0,0])
    linear_extrude(1)
    text(txt, rectHeight-1, "FreeSerif", "center", "center", halign="center");
makesuport();
```

![The 3d model of the hole tester](./images/hole.png)  
[scad file](files/scad/mesureTool/hole.scad)

### Ruler

The ruler is straightforward, I make a cube for the base then I loop on the steps I want and add a line for each step.

``` scad
steps = [0.1, 1];
rulerWidth = 3;
rulerHeight = 0.1;
rulerLenght = 10;
cube([rulerWidth, rulerLenght, rulerHeight]);
for (step = steps)
    for (i = [0:step:rulerLenght])
        translate([0, i, 0])
            cube([1, 0.01 , rulerHeight+0.1 * step/2]);

txt = str(steps);
translate([rulerWidth*2/3,0,rulerHeight])
    rotate([0,0,90])
    linear_extrude(rulerHeight/10)
    text(txt, rulerWidth/3, "FreeSerif");
```

For both tools, I also added text to know what the units tested.
![The 3d model of the ruler](./images/ruler.png)  
[scad file](files/scad/mesureTool/ruler.scad)

## Note

- better chances with using `apt` to install the tools than via store because of some dependencies.
