module sector(radius, angles, fn = 24) {
    r = radius / cos(180 / fn);
    step = -360 / fn;

    points = concat([[0, 0]],
        [for(a = [angles[0] : step : angles[1] - 360]) 
            [r * cos(a), r * sin(a)]
        ],
        [[r * cos(angles[1]), r * sin(angles[1])]]
    );

    difference() {
        circle(radius, $fn = fn);
        polygon(points);
    }
}

module arc(radius, angles, width = 1, fn = 24) {
    difference() {
        sector(radius + width, angles, fn);
        sector(radius, angles, fn);
    }
} 

largeur = 50;
longeur = 100;

translate([0,-longeur*0.45, 0]){
    cube([2, longeur*2, 3], center=true);
arc(largeur/2 - 4, [0, 180]);

}
difference(){
    cube([largeur, longeur, 5], center = true);
    cube([largeur-4, longeur-4, 6], center = true);
    cube([2.5, longeur*2, 3.5], center=true);
    
}
for (i = [-longeur/2+5:longeur/13:longeur/2])
    translate([largeur/2-2, i-0.2,0]) sector(2, [180,270]);

for (i = [-longeur/2+5:longeur/13:longeur/2])
    translate([-largeur/2+2, i-0.2,0]) sector(2, [270,360]);