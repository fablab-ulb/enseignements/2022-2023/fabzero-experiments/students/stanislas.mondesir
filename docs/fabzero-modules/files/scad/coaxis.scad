/*
@File    :   coaxis.scad
@Time    :   2023/02/27 14:55:55
@Author  :   Stanislas Mondesir 
@Version :   1.0
@License :   Creative Commons Attribution-ShareAlike 4.0 International License.
@Desc    :   None
*/
nbHole = 4;

diametreHole = 5;
spaceBetween = 3;
border = 2;
lPHeight = 2;
pieceDist = 20;
include <legoPart.scad>
center = border/2 + diametreHole/2;
translate ([center,0, 0])
    makesuport();

rectLenght = border+diametreHole*(nbHole)+spaceBetween*(nbHole-1);

pieceWidth = diametreHole;
y1 = diametreHole/2;
y2 = diametreHole/2 + pieceDist;
x1 = diametreHole/2;
x2 = rectLenght - diametreHole/2;
include <line.scad>
linear_extrude(lPHeight/2){
line([x1,y1], [x2,y2]);
line([x1,y2], [x2,y1]);
}
translate([center,pieceWidth + pieceDist,0])
    makesuport();

