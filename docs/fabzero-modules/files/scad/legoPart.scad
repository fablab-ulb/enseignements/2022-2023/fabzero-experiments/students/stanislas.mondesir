/*
@File    :   legoPart.scad
@Time    :   2023/02/27 15:51:38
@Author  :   Stanislas Mondesir 
@Version :   1.0
@License :   Creative Commons Attribution-ShareAlike 4.0 International License.
@Desc    :   None
*/
module makesuport(){
    realSpaceBetween = spaceBetween-border;

    difference(){
        hull(){
            cylinder(d=diametreHole+border, h=lPHeight);
            translate([(diametreHole+border+realSpaceBetween)*(nbHole-1), 0,0])
                cylinder(d=diametreHole+border, h=lPHeight);
        }
        for (i=[0:nbHole-1]){
            translate([(border + diametreHole + realSpaceBetween) * i,0,-1]){
                cylinder(h=lPHeight+5, d=diametreHole);
            }
            translate([(border + diametreHole + realSpaceBetween) * i,0,lPHeight])
                sphere(d=diametreHole*1.07);
            translate([(border + diametreHole + realSpaceBetween) * i,0,0])
                sphere(d=diametreHole*1.07);
        }
    }
}

module makesuportSlotted(){
    realSpaceBetween = spaceBetween-border;

    difference(){
        hull(){
            cylinder(d=diametreHole+border, h=lPHeight);
            translate([(diametreHole+border+realSpaceBetween)*(nbHole-1), 0,0])
                cylinder(d=diametreHole+border, h=lPHeight);
        }
        hull(){
            cylinder(h=lPHeight+5, d=diametreHole);
            translate([(border + diametreHole + realSpaceBetween) * (nbHole-1),0,-1])
                    cylinder(h=lPHeight+5, d=diametreHole);
        }
    }
}