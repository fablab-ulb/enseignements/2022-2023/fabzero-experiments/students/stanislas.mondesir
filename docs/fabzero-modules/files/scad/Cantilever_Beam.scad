/*
@File    :   Cantilever_Beam.scad
@Time    :   2023/02/23 21:48:58
@Author  :   Stanislas Mondesir 
@Version :   1.0
@License :   Creative Commons Attribution-ShareAlike 4.0 International License.
@Desc    :   None
*/

nbHole = 2;

diametreHole = 5;
spaceBetween = 3;
border = 2;
lPHeight = 2;

rodeLenght = 30;
rodeHeight = 0.3;

include <legoPart.scad>

rodeWidth = border+diametreHole-border;
rectLenght = border+diametreHole*(nbHole)+spaceBetween*(nbHole-1);
center = border/2 + diametreHole/2;
makesuport();

translate([rectLenght-center-border/2, -rodeWidth/2,0])
    cube([rodeLenght+border, rodeWidth, rodeHeight]);
