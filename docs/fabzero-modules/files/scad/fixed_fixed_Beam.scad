/*
@File    :   fixed_fixed_Beam.scad
@Time    :   2023/02/23 19:21:11
@Author  :   Stanislas Mondesir 
@Version :   1.0
@License :   Creative Commons Attribution-ShareAlike 4.0 International License.
@Desc    :   None
*/
$fn = 50;
nbHole = 2;

diametreHole = 5.0;
spaceBetween = 3;
border = 2;
lPHeight = 5;
rodeWidth = 1.5;

rodeLenght = 40;
rodeHeight = 5;//1.8


include <legoPart.scad>

// rodeWidth = border+diametreHole-border;
lPLenght = border+diametreHole*(nbHole)+spaceBetween*(nbHole-1);
center = border/2 + diametreHole/2;
makesuport();

translate([lPLenght-center-border/2, -rodeWidth/2,0])
    cube([rodeLenght+border, rodeWidth, rodeHeight]);


translate([lPLenght+rodeLenght, 0,0]) 
    makesuport();
