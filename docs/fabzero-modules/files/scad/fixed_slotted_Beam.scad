/*
@File    :   fixed_slotted_Beam.scad
@Time    :   2023/02/23 21:46:00
@Author  :   Stanislas Mondesir 
@Version :   1.0
@License :   Creative Commons Attribution-ShareAlike 4.0 International License.
@Desc    :   None
*/

nbHole = 2;

diametreHole = 5.1;
spaceBetween = 2;
border = 2;
lPHeight = 3;
rodeLenght = 20;
rodeHeight = 3;

include <legoPart.scad>

rodeWidth = 0.5;
rectLenght = border+diametreHole*(nbHole)+spaceBetween*(nbHole-1);
center = border/2 + diametreHole/2;
makesuport();

translate([rectLenght-center-border/2, -rodeWidth/2,0])
    cube([rodeLenght+border, rodeWidth, rodeHeight]);


translate([rectLenght+rodeLenght, 0,0]) 
    makesuportSlotted();
