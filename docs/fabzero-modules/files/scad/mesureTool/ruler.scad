/*
@File    :   ruler.scad
@Time    :   2023/02/25 12:49:44
@Author  :   Stanislas Mondesir 
@Version :   1.0
@License :   Creative Commons Attribution-ShareAlike 4.0 International License.
@Desc    :   None
*/


steps = [1, 10];
rulerHeight = 3;
nbUnit = 10;
rulerLenght = (nbUnit) * max(steps);
rulerWidth = rulerLenght/4;

module latte(){
    union(){
         cube([rulerWidth, rulerLenght, rulerHeight]);
    }
}
difference(){

latte();

for (step = steps){
    for (i = [0:step:rulerLenght-0.1]){
        translate([0, i, rulerHeight/2]){
            cube([rulerWidth*3/5 * step/5, max(steps)/15, rulerHeight*1.4]);
        }
        // translate([rulerWidth, i, 0]){
        //     color([0,1,0,1])linear_extrude(rulerHeight*1.1) text(str(i/step), step/10, "FreeSerif",halign="right");
        //     }
        }
}
}


txt = str(steps);
translate([rulerWidth*3/5,0,0])
    linear_extrude(rulerHeight*1.2)
    rotate([0,0,90])
    text(txt, rulerWidth/3,valign="top",  "FreeSerif");