/*
@File    :   hole.scad
@Time    :   2023/02/25 09:10:37
@Author  :   Stanislas Mondesir 
@Version :   1.0
@License :   Creative Commons Attribution-ShareAlike 4.0 International License.
@Desc    :   None
*/

$fn = 50;
nbHoleEachSide = 4;
BaseddiametreHole = 4.8;
testStep = 0.1;
maxDiameter = BaseddiametreHole + testStep * nbHoleEachSide;
space = 3.2;
border = 2;
rectHeight = 1.8;

Realspace = space-border;

module makesuport(){
    difference(){
        hull(){
            translate([(maxDiameter+border+Realspace)*-nbHoleEachSide, 0,0])
                cylinder(d=maxDiameter+border, h=rectHeight);
            translate([(maxDiameter+border+Realspace)*nbHoleEachSide, 0,0])
                cylinder(d=maxDiameter+border, h=rectHeight);
        }
        for (i=[-nbHoleEachSide:nbHoleEachSide]){
            translate([(border + maxDiameter + Realspace) * i,0,-1]){
                cylinder(h=rectHeight+5, d=BaseddiametreHole+(i)*testStep);
            }
        }
    }
}

translate([0,(border + maxDiameter)/2,0])
    cube([1, 1, rectHeight]);

txt = str(str(BaseddiametreHole), ":" , str(testStep));
translate([0,-((border + maxDiameter)/2),0])
    rotate([90,0,0])
    linear_extrude(rectHeight/7)
    text(txt, rectHeight*0.9, "FreeSerif", halign="center");
makesuport();