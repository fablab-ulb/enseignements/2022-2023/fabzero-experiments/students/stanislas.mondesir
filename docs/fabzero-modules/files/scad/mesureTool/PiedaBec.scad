/*
@File    :   ruler.scad
@Time    :   2023/02/25 12:49:44
@Author  :   Stanislas Mondesir 
@Version :   1.0
@License :   Creative Commons Attribution-ShareAlike 4.0 International License.
@Desc    :   None
*/


steps = [10, 100];
rulerHeight = 3;
nbUnit = 10;
rulerLenght = (nbUnit) * max(steps);
rulerWidth = rulerLenght/10;

module latte(){
    union(){
        cube([rulerWidth*1.5, rulerLenght, rulerHeight*0.5]);
        translate([rulerWidth/4, 0,0]) cube([rulerWidth, rulerLenght, rulerHeight]);
    }
}
translate([-rulerWidth/4,0,0]){
    translate([0,rulerLenght*0.1,0]) latte();
    color([1, 0,0,1])
        cube([rulerWidth*2, rulerLenght*0.1,rulerHeight*1.5]);
}
translate([0, rulerLenght * 1.2,0])
color([1, 1,0,1]) difference(){
    cube([rulerWidth*2, rulerLenght*0.1,rulerHeight*2]);
    translate([0.1,-0.1,0.05]) scale(1.2)  latte();
    translate([0.12+(rulerWidth*1.2)/4, 0,0.1])cube([rulerWidth*1.2, rulerLenght, 1]);
}

for (step = steps){
    for (i = [0:step:rulerLenght-0.1])
        translate([0, i+rulerLenght*0.1, 0]){
            cube([rulerWidth*3/5, step/50, rulerHeight+0.1 * step/2]);
    }
}
step = max(steps);

for (i = [0:step:rulerLenght-0.1])
        translate([rulerWidth, i+rulerLenght*0.1, rulerHeight]){
            color([0,1,0,1])linear_extrude(rulerHeight/10) text(str(i/step), step/10, "FreeSerif",halign="right");
            }

txt = str(steps);
translate([0,rulerLenght*0.03*0.5,rulerHeight])
    linear_extrude(rulerHeight*1.7)
    text(txt, rulerLenght*0.03, halign="left", "FreeSerif");