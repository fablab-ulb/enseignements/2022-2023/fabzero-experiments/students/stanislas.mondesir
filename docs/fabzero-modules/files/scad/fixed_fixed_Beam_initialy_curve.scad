/*
@File    :   fixed_fixed_Beam.scad
@Time    :   2023/02/23 19:21:11
@Author  :   Stanislas Mondesir 
@Version :   1.0
@License :   Creative Commons Attribution-ShareAlike 4.0 International License.
@Desc    :   None
*/

nbHole = 2;

diametreHole = 5;
spaceBetween = 3;
border = 2;
lPHeight = 2;
rodeHeight = 2;

radius = 20;
angles = [0, 90];
width = 0.3;
fn = 100;


include <legoPart.scad>


rodeWidth = border+diametreHole-border;
rectLenght = border+diametreHole*(nbHole)+spaceBetween*(nbHole-1);
center = border/2 + diametreHole/2;



rotate([0,0,angles[0]]) translate([center+radius-rodeWidth/2, -center+border/2, 0]) rotate([0,0,-90]) 
    makesuport();

//src: https://openhome.cc/eGossip/OpenSCAD/SectorArc.html
module sector(radius, angles, fn = 24) {
    r = radius / cos(180 / fn);
    step = -360 / fn;

    points = concat([[0, 0]],
        [for(a = [angles[0] : step : angles[1] - 360]) 
            [r * cos(a), r * sin(a)]
        ],
        [[r * cos(angles[1]), r * sin(angles[1])]]
    );

    difference() {
        circle(radius, $fn = fn);
        polygon(points);
    }
}

module arc(radius, angles, width = 1, fn = 24) {
    difference() {
        sector(radius + width, angles, fn);
        sector(radius, angles, fn);
    }
} 

linear_extrude(rodeHeight) arc(radius, angles, width);

rotate([0,0,angles[1]-5]) translate([center+radius-rodeWidth/2, center+border/2, 0]) rotate([0,0, 90]) makesuport();
