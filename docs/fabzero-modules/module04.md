# 4. Laser Cutting  

## Machine used

### Epilogue  

#### Spec  

- surface: 81x50 cm  
- max height: 31cm

#### Cut

1. Start epilog by turning the key.
2. Start the ventilator.
3. Transfer your design to the computer.
4. Print your design, as an svg, and select Epilog as the printer.
5. Select autoFocus.
6. Select parameters, divided by color.
7. Launch.
8. Start the cut on the epilog with the play button.
9. Stay beside the epilog until the end in case the material burns.

#### Parameter

Epilog propose 2 types of cut,  

- Engrave: which will work more like a basic printer going layer by layer.
- Vector: the preferred of the two which will follow the path given by the svg.

we will mostly use Vector but we will play with the parameter.

- frequency: we were told it was better to leave it at 30.
- Power: the intensity of the laser, the greater it is the deeper it will go but the cut will be less precise.
- Speed: the speed of the laser movement.

If the material is not cut on the first go you can, without moving the material restart the cutter.
To know what parameter to use it is best to do a test beforehand on your material.

## Test file

I first made a test file so I will know the parameter to use for the material. To make it I first made a matrice 5x5 of cubes in Inkscape (Align and distribute>grid>Arrange) I saved it as an optimized svg, then open it in vs code and change the color of each square.

[test file](files/TestA4.svg)
![test file](images/testfile2.jpeg)

## Design an origami

For this design, I've decided to go from a problem to find and find a real solution.

### The problem

Storing vinyl, right now in my house, the vinyl is stored in a box and it would be good to have a nicer way to stock it.

### Prototype

First I made a simple paper prototype adapting the steps found [here](https://www.wikihow-fun.com/Make-an-Origami-Triangle-Base) to see what it would look like.
![prototype](images/proto_orig.jpeg)

### Design cut

Then, I designed the folds and cuts in Inkscape, giving a different color to differentiate the two.
![the design file](files/plieOrigamiA4.svg)
After cutting the cardboard I got this:
![the cut flat](images/cut_2.jpeg)
And Just need to fold it:
![the cut](images/cut_1.jpeg)

### Second design

During the design I rescaled my file and so change the angle, changing the 90 degrees needed so I need to be more vigilant about that.
I also chose the intensity of the laser a bit too strong so the design broke so I need to decrease it.
So taking all that into account I recreate the design to only have right angles. Leaving me with that final design:
![the second design](files/dessin.svg)
And that origami:
![the origami](images/vin.jpeg)
![the origami in use](images/VINYL.jpeg)
